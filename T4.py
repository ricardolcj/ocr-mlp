from sklearn.datasets import fetch_openml
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold
import numpy as np
import tensorflow as tf
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.callbacks import EarlyStopping
from pretty_confusion_matrix import pp_matrix_from_data

tf.random.set_seed(100)

lr = 0.01
max_epocas = 100
tamanho_teste = 0.3
tamanho_validacao = 0.2
max_erros_validacao = 6

def leitura():
	try:

		y = np.load("y.npy", allow_pickle=True)
		X = np.load("X.npy", allow_pickle=True)

	except FileNotFoundError:

		X, y = fetch_openml('mnist_784', version=1, return_X_y=True, as_frame=False)

		np.save("y", y)
		np.save("X", X)

	return X, y

X, y = leitura()

#Remove variância nula
X = VarianceThreshold().fit_transform(X)
classes = sorted(np.unique(y))

#OneHotEncoding
y = to_categorical(y)

x_treino, x_teste, y_treino, y_teste = train_test_split(X, y, test_size=0.3, random_state=100)

n_atributos = x_treino.shape[1]

model = Sequential()
model.add(Dense(80, activation='sigmoid', kernel_initializer='he_normal', input_shape=(n_atributos,)))
model.add(Dense(80, activation='relu', kernel_initializer='he_normal'))
model.add(Dense(10, activation='softmax'))


es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=max_erros_validacao, restore_best_weights=True)

sgd = SGD(learning_rate=lr)
model.compile(optimizer=sgd, loss='binary_crossentropy', metrics=['accuracy'])

model.fit(X, y, epochs=max_epocas, batch_size=35, callbacks=[es], validation_split=tamanho_validacao)

#Converte OneHotEncoding para decimal para o plot da Matriz
y_pred = np.argmax (model.predict (x_teste), axis = -1)
y_teste = np.argmax(y_teste, axis = -1)

pp_matrix_from_data(y_teste, y_pred, cmap="tab20", columns=classes)